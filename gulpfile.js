var gulp = require('gulp');
var spawn = require('child_process').spawn;

gulp.task('run-umajin', function (cb) {
  var cmd = spawn('umajin.exe', ['start.u', '--log-format=t:l:s', '--log-level=DEBUG'], {stdio: 'inherit'});
  cmd.on('close', function (code) {
    cb(code);
  });
});